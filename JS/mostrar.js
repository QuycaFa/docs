const Cambiar = (IdBoton, IdObjeto, clase, accion)=>{
  document.getElementById(IdBoton).addEventListener('click', () => document.getElementById(IdObjeto).classList.toggle(clase));
  document.getElementById(IdBoton).addEventListener('click', accion);
};

export const RichText = (id)=>{
  $().ready(function () {
    $(id).richText({
      leftAlign:false,
      centerAlign:false,
      rightAlign:false,
      fonts:false,
      fontColor:false,
      fileUpload:false,
      Embed:false
    });
})};

export default Cambiar;