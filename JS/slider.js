
const Slider = (contenedor, selector, etiqueta) => {
  $(window).load(function () {
		$(contenedor).flexslider({
			animation: "slide",
			selector: selector + " > " + etiqueta,
			directionNav: true,
		});
	});
};

export const Carrousel = (contenedor, iWidth)=>{
	$(window).load(function() {
		$(contenedor).flexslider({
			animation: "slide",
			directionNav: true,
			animationLoop: true,
			itemWidth: iWidth,
			itemMargin: 5
		});
	});
};

export default Slider;
