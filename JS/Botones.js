import {
	CambiarSC
} from './articulos.js'
import VM from './vm.js'
import Id from './Id.js'

export const Botones = ()=>{
	VM(Id('arte'), 'contenido', 'oculto1', 'w3-hide');
	VM(Id('sports'), 'contenido', 'oculto1', 'w3-hide');
	VM(Id('entintados'), 'b1', 'oculto', 'w3-hide');
	VM(Id('entintados'), 'contenido', 'oculto1', 'w3-hide');
	VM(Id('mundohoy'), 'contenido', 'oculto1', 'w3-hide');
	VM(Id('infantil'), 'b1', 'oculto', 'w3-hide', () => {
		CambiarSC('b1', 'w3-hide');
		CambiarSC('b2', 'w3-hide');
	});
	VM(Id('infantil'), 'b2', 'oculto', 'w3-hide', () => {
		CambiarSC('b1', 'w3-hide');
		CambiarSC('b2', 'w3-hide');
	});
	VM(Id('ojocientifico'), 'contenido', 'oculto1', 'w3-hide');
	VM(Id('entrevoces'), 'b1', 'oculto', 'w3-hide', () => {
		CambiarSC('b1', 'w3-hide');
		CambiarSC('b2', 'w3-hide');
	});
	VM(Id('entrevoces'), 'b2', 'oculto', 'w3-hide', () => {
		CambiarSC('b1', 'w3-hide');
		CambiarSC('b2', 'w3-hide');
	});
	VM(Id('entrevoces'), 'contenido', 'oculto1', 'w3-hide');
	VM(Id('huerta'), 'contenido', 'oculto1', 'w3-hide');
	VM(Id('podcast'), 'contenido', 'oculto1', 'w3-hide');
	VM(Id('profes'), 'contenido', 'oculto1', 'w3-hide');
	VM(Id('padres'), 'contenido', 'oculto1', 'w3-hide');
}
