const Alerta = (parametros, mes, dia, horaInicio, horaFin) => {
	const fechaInicio = new Date(2020, mes, dia, horaInicio).getTime();
	const fechaFin = new Date(2020, mes, dia, horaFin).getTime();

	if (Date.now() > fechaInicio && Date.now() < fechaFin) {
		Swal.fire({
			icon: parametros.icono,
			title: parametros.titulo,
			html: parametros.htmlM,
			footer: parametros.pieHtml,
			width: parametros.ancho,
			position: parametros.ubicacion,
			showClass: {
				popup: parametros.entrada,
			},
			hideClass: {
				popup: parametros.salida,
			}
		});
	};
}

export default Alerta;
