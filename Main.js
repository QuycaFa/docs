import Alerta from './JS/alerta.js';
import Cambiar, {
	RichText
} from './JS/articulos.js'
import Slider, {Carrousel} from './JS/slider.js';
import Id from './JS/Id.js';
import {
	Botones
} from './JS/Botones.js';

//botones ver mas
Botones();

//Editor
if (Id('index')) {
	Cambiar('mostrarR', 'file', 'w3-hide', () => {
		Id("fe").classList.toggle("w3-hide");
		if (Id("textr").classList.toggle("w3-hide") == false) {
			Id("textr").classList.toggle("w3-hide");
		};
	});

	Cambiar('mostrarR2', 'textr', 'w3-hide', () => {
		if (Id("file").classList.toggle("w3-hide") === false &
			Id("fe").classList.toggle("w3-hide") === false) {
			Id("file").classList.toggle("w3-hide");
			Id("fe").classList.toggle("w3-hide");
		};
	});
	RichText('#RichText');
};

//Slider
if (Id('arte')) {
	Carrousel(".flexslider", 210);
};
if (Id('index')) {
	Slider(".flexslider", '.slides', 'li');
};
//Alertas

const infoAlerta = {
	icono: 'info',
	titulo: 'VII Encuentro de narrativas e imágenes ',
	htmlM: '<p>El Colegio Integrado Campestre Colombia Hoy junto al Periódico Quyca Fa traerán el <b>VII Encuentro de narrativas e imágenes</b> del 28 de septiembre al 2 de octubre para que puedas acceder a todo el contenido de calidad desde nuestra página web.</p>',
	ancho: '50%',
	botonCerrar: true,
	infoBoton: 'cerrar',
	entrada: 'animate__animated animate__zoomIn',
	salida: 'animate__animated animate__bounceOutRight',
}

if (Id('index')) {
	Alerta(infoAlerta, 7, 20, 22, 23);
};
